/* main.h - Shell Simulator - EGM shell

   Copyright (c) 2015	, Guilherme T. T. Pinto <gttpinto@usp.com>
			, Eduardo C. de O. Junior <eduardojr@grad.icmc.usp.br>
			, Mateus S. Rodrigues <mateusser@grad.icmc.usp.br>

   This file is part of POSIXeg.

   POSIXeg is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <foosh.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <debug.h>

#define PROMPT "@:"
#define RUNNING 0
#define WAITING 1

/* void test(void); */

typedef struct{
	int pid;
	int status;
	int ground;
} job;

typedef struct{
	job* jobs;
	int count;
} s_runningJobs;

int go_on = 1;			/* This variable controls the main loop. */
char *cwd;				/* current working directory */
s_runningJobs* runningJobs; /* List of running jobs */

/* Built-in jobs command */
void jobs(){
	int i;

	printf("PID\tstatus\tground\n");
	printf("--------------------------\n");

	for (i = 0; i < runningJobs->count; i++){
		printf("%d\t%s\t%s\n", runningJobs->jobs[i].pid, !runningJobs->jobs[i].status ? "RUNNING" : "WAITING", !runningJobs->jobs[i].ground ? "FOREGROUND" : "BACKGROUND");
	}
}

/* Built-in change directory command */
void cd(char* command[]){
	/* Checks for a argument */
		if (command[1] != NULL){ 
			/* Tries to change the directory */
			if (chdir(command[1]) < 0){ 
				/* Failed to change directory */
				printf("Error: %s is not a valid directory\n", command[1]);
			} else { 
				/* Directory changed */

				if (cwd != NULL) free(cwd);
				cwd = (char*)malloc(1024*sizeof(char)); /* Stores the current working directory */

				cwd = getcwd(cwd, 1024); /* Gets current working directory */
				setenv("PWD", cwd, 1); /* Sets PWD as the current working directory */
			}

		} else { /* No argument found, prints help */
			printf("Usage: cd [directory]\n");
		}
}

/* Built-in fg command */
void fg(char* command[]){
	/* procura o processo pra mandar ao foreground */
	int i, status;
	for (i = 0; i < runningJobs->count; i++){
		if (runningJobs->jobs[i].pid == atoi(command[1])){
			runningJobs->jobs[i].ground = FOREGROUND;
			wait(&status);
		}
	}
}

/* Runs a built in command */
int execBuiltIn(char* command[]){
	if (strcmp(command[0], "exit") == 0){	/* Running "exit" */
		go_on = 0;	/* Lowers the mainloop flag */
		return 1;
	}
	if (strcmp(command[0], "cd") == 0){		/* Running "cd" */
		cd(command);
		return 1;
	}
	if (strcmp(command[0], "jobs") == 0){ /* Running "jobs" */
		jobs();
		return 1;
	}
	if (strcmp(command[0], "fg") == 0){ /* Running "fg" */
		fg(command);
		return 1;
	}
	return 0;
}

/* Creates a job */
job createJob(int pid, int ground){
	job newJob;
	newJob.pid = pid;
	newJob.ground = ground;
	newJob.status = RUNNING;
	return newJob;
}

/* Adds jobs to the job list and executes them */
void addJobs(pipeline_t* pipeline){
	int i, aux;
	int pid, status;

	/* allocates space for the new jobs */
	runningJobs->jobs = (job*)realloc(runningJobs->jobs, (runningJobs->count + pipeline->ncommands)*sizeof(job));

	/* Adds all jobs to the list and executes them */
	for (i = 0; i < pipeline->ncommands; i++){
		/* Redirects the input of stdin for the first process */
		if (REDIRECT_STDIN(pipeline) && i == 0){
			close(0);
			fatal(open(pipeline->file_in, O_RDONLY, S_IRUSR | S_IWUSR) < 0, NULL);
		}
		/* Redirects the output from stdout for the last process */
		if (REDIRECT_STDOUT(pipeline) && i == pipeline->ncommands-1){
			close(1);
			fatal(open(pipeline->file_out, O_CREAT | O_TRUNC | O_RDWR, S_IRUSR | S_IWUSR) < 0, NULL);
		}

		

		pid = fork(); /* Forks the process */
		fatal(pid<0,NULL);

		if (pid > 0) { /* Parent */
			/* Adds the job to the list */
			runningJobs->jobs[runningJobs->count + i] = createJob(pid, pipeline->ground);
			runningJobs->count += 1;

			/* Waits, if the process is foreground */
			if (RUN_FOREGROUND(pipeline)){
				aux = wait (&status); /* Waits for the child to end */
				fatal (aux<0, NULL);
			}
		} else { /* Child */
			aux = execvp (pipeline->command[i][0], pipeline->command[i]);
			fatal(aux < 0, "Could not find program");
		}		
	}
}

int main (int argc, char **argv)
{
  buffer_t *command_line;
  int aux;
  int tempFDOut, tempFDIn;

  pipeline_t *pipeline;

  runningJobs = (s_runningJobs*)malloc(sizeof(s_runningJobs));
  runningJobs->jobs = NULL;
  runningJobs->count = 0;

  command_line = new_command_line ();
  /* test(); */
  
  pipeline = new_pipeline ();

  cwd = (char*)malloc(1024*sizeof(char)); /* Stores the current working directory */
  cwd = getcwd(cwd, 1024); /* Gets current working directory */

  printf("\n\nWelcome to the EGM shell!!\n\n");

  while (go_on)
    {
      printf ("%s %s ", cwd, PROMPT);
      fflush (stdout);
      aux = read_command_line (command_line);
      fatal (aux<0, "Error reading command line");

      /* Parse command line. */

      if (!parse_command_line (command_line, pipeline) || 1)
		{	
			/* Does nothing if no command was written */
			if (pipeline->ncommands == 0) continue;

			/* Runs a built-in command. */
			if (execBuiltIn(pipeline->command[0])) continue;

			/* Saves stdout/in on a temp variable */
			tempFDOut = dup(1);
			tempFDIn = dup(0);

			/* Adds and executes the jobs */
			addJobs(pipeline);

			/* restores stdout and stdin */
			close(1);
			dup(tempFDOut);
			close(tempFDOut);
			close(0);
			dup(tempFDIn);
			close(tempFDIn);
		}
    }

  release_command_line (command_line);
  release_pipeline (pipeline);

  return EXIT_SUCCESS;
}
